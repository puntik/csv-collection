<?php

use Abetzi\CsvCollection\CsvCollection;
use PHPUnit\Framework\TestCase;

class CsvCollectionTest extends TestCase
{

	/**
	 * @test
	 */
	public function it_fails_with_exception_when_input_file_does_not_exist()
	{
		// Given
		$file = 'file_does_not_exist.csv';

		// Expected exception
		$this->expectException(InvalidArgumentException::class);

		// When
		CsvCollection::fromFile($file)->make();
	}

	/**
	 * @test
	 */
	public function it_fails_when_input_file_is_not_writable()
	{
		self::markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function it_returns_csvcollection_instance()
	{
		self::markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function it_counts_lines_when_header_is_used()
	{
		self::markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function it_counts_lines_when_header_is_not_used()
	{
		self::markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function it_takes_correct_number_of_lines_when_length_is_used()
	{
		self::markTestIncomplete();
	}

	/**
	 * @test
	 */
	public function it_splits_line_with_used_delimiter()
	{
		self::markTestIncomplete();
	}
}
