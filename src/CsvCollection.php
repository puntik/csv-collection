<?php declare(strict_types = 1);

namespace Abetzi\CsvCollection;

use Illuminate\Support\LazyCollection;

class CsvCollection implements \Countable
{

    /** @var string */
    private $filename;

    /** @var bool */
    private $useHeader;

    /** @var string */
    private $delimiter;

    /** @var int */
    private $length;

    private function __construct($filename)
    {
        $this->filename  = $filename;
        $this->useHeader = true;
        $this->delimiter = ',';
        $this->length    = 0;
    }

    /**
     * @param string $filename
     *
     * @return CsvCollection
     * @throws \Throwable
     */
    public static function fromFile(string $filename): self
    {
        throw_unless(
            is_file($filename),
            new \InvalidArgumentException(sprintf('No "%s" file found.', $filename))
        );

        throw_unless(
            is_readable($filename),
            new \InvalidArgumentException(sprintf('File "%s" is not readable.', $filename))
        );

        return new self($filename);
    }

    public function useHeader(bool $useHeader = true): self
    {
        $this->useHeader = $useHeader;

        return $this;
    }

    public function delimiter(string $delimiter = ','): self
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    public function length(int $length = 0): self
    {
        $this->length = $length;

        return $this;
    }

    public function make(): LazyCollection
    {
        return LazyCollection::make(function () {
            $handle = fopen($this->filename, 'rb');

            if ($this->useHeader) {
                // use first line can as a header
                $header = fgetcsv($handle, $this->length, $this->delimiter);
            }

            while (($line = fgetcsv($handle, $this->length, $this->delimiter)) !== false) {
                // compare size of header to size of elements on line
                throw_if(
                    $this->useHeader && count($header) !== count($line),
                    new \InvalidArgumentException('Size of header is different then size of line.')
                );

                yield $this->useHeader ? array_combine($header, $line) : $line;
            }

            fclose($handle);
        });
    }

    /**
     * Experimental usage, use collection->count() instead. Do not use it yet.
     *
     * @return int
     */
    public function count(): int
    {
        if (PHP_OS === 'Linux') {
            return (int) exec("wc -l '$this->filename'");
        }

        $handle = fopen($this->filename, 'rb');
        $lines  = 0;

        while (! feof($handle)) {
            $lines += substr_count(fread($handle, $this->length), "\n");
        }

        fclose($handle);

        return $lines;
    }
}

