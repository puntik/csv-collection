## About

Library provides a simple way to read input csv file as laravel collection. 
It implements [LazyCollection](https://laravel.com/docs/8.x/collections#lazy-collections) to deffered access to rows.  

## Install 

via composer
~~~
composer require abetzi/csv-collection
~~~
## Usage
~~~
<?php
/** @var CsvCollection $collection */
$collection = CsvCollection::fromFile('data.csv') // data source
    ->delimiter(',')                              // optional
    ->useHeader(true)                             // optional
    ->make();                                     // creates a new lazy collection

~~~

and then use collection as described in [documentation](https://laravel.com/docs/8.x/collections).
~~~
<?php
// basic usage
foreach($collection as $row) {
    // do something useful
}

// or better 
$collection->each(function($row) {
    // do something useful with $row
});
~~~
